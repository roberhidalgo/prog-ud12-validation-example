package es.progcipfpbatoi.exceptions;

public class ControlNotFoundException extends Exception{
    public ControlNotFoundException(String fxId) {
        super("El componente " + fxId + " no ha sido encontrado");
    }
}
