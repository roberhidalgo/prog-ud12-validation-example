package es.progcipfpbatoi.exceptions;

public class ValidationException extends Exception{

    public ValidationException(String message) {
        super(message);
    }
}
