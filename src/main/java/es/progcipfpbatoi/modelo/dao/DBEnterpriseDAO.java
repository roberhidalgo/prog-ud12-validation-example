package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Enterprise;
import es.progcipfpbatoi.services.MySqlConnectionService;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DBEnterpriseDAO implements EnterpriseDAOInterface{

    private static final String TABLE_NAME = "Enterprise";
    @Override
    public List<Enterprise> findAll() {
        String sql = String.format("SELECT * FROM %s", TABLE_NAME);

        ArrayList<Enterprise>empresas = new ArrayList<>();

        try (
                Connection connection =  new MySqlConnectionService().getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {

            while(resultSet.next()) {
                Enterprise enterprise = getEnterpriseFromResultSet(resultSet);
                empresas.add(enterprise);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return empresas;
    }

    private Enterprise getEnterpriseFromResultSet(ResultSet resultSet) throws SQLException{
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String address = resultSet.getString("address");
        String city = resultSet.getString("city");
        String province = resultSet.getString("province");
        String country = resultSet.getString("country");
        LocalDate createdOn = resultSet.getDate("createdOn").toLocalDate();
        String nif = resultSet.getString("nif");

        return new Enterprise(id, name, address, city, province, country,createdOn, nif);
    }

    @Override
    public List<String> findAllDistinctDataFromField(String field) {
        ArrayList<String> allData = new ArrayList<>();
        String sql = String.format("SELECT DISTINCT %s FROM %s", field, TABLE_NAME);

        try (
                Connection connection =  new MySqlConnectionService().getConnection();
                PreparedStatement statement = connection.prepareStatement(sql);
                ResultSet resultSet = statement.executeQuery(sql);
        ) {

            while(resultSet.next()) {
                String data = resultSet.getString(field);
                allData.add(data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return allData;

    }

    @Override
    public Enterprise findById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id = ?",TABLE_NAME);
        Enterprise enterprise = null;
        try (
                Connection connection =  new MySqlConnectionService().getConnection();
                PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        ) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                enterprise = getEnterpriseFromResultSet(resultSet);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return enterprise;

    }

    public Enterprise save(Enterprise enterprise) {
        if (findById(enterprise.getId()) == null) {
            return insert(enterprise);
        } else {
            return update(enterprise);
        }
    }

    private Enterprise insert(Enterprise enterprise) {
        String sql = String.format("INSERT INTO %s (name, address, city, province, country, createdOn, nif) VALUES (?,?,?,?,?,?,?)",
                TABLE_NAME);

        try (
                Connection connection =  new MySqlConnectionService().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ) {
            preparedStatement.setString(1, enterprise.getName());
            preparedStatement.setString(2, enterprise.getAddress());
            preparedStatement.setString(3, enterprise.getCity());
            preparedStatement.setString(4, enterprise.getProvince());
            preparedStatement.setString(5, enterprise.getCountry());
            preparedStatement.setDate(6, Date.valueOf(enterprise.getCreatedOn()));
            preparedStatement.setString(7, enterprise.getNif());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                enterprise.setId(resultSet.getInt(1));
            }

            return enterprise;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Enterprise update(Enterprise enterprise) {
        String sql = String.format("UPDATE %s SET name = ?, address = ?, city = ?, province = ?, country = ?, nif = ? WHERE id = ?",
                TABLE_NAME);

        try (
                Connection connection =  new MySqlConnectionService().getConnection();
                PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, enterprise.getName());
            statement.setString(2, enterprise.getAddress());
            statement.setString(3, enterprise.getCity());
            statement.setString(4, enterprise.getProvince());
            statement.setString(5, enterprise.getCountry());
            statement.setString(6,enterprise.getNif());
            statement.setInt(7, enterprise.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return enterprise;
    }


}
