package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.Enterprise;

import java.util.List;

public interface EnterpriseDAOInterface {

    List<Enterprise>findAll();

    List<String> findAllDistinctDataFromField(String field);
    Enterprise findById(int id);
    Enterprise save(Enterprise enterprise);
}
