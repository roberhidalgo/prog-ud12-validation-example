package es.progcipfpbatoi.modelo.dto;

import java.time.LocalDate;

public class Enterprise {
    private int id;
    private String name;
    private String address;
    private String city;
    private String province;
    private String country;
    private LocalDate createdOn;
    private String nif;

    public Enterprise(int id, String name, String address, String city, String province, String country, LocalDate createdOn, String nif) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.country = country;
        this.createdOn = createdOn;
        this.nif = nif;
    }

    public Enterprise(String name, String address, String city, String province, String country, String nif) {
        this(-1, name,address,city,province,country, LocalDate.now(), nif);
    }

    public Enterprise(int id) {
        this(id, "","","","","", LocalDate.now(), "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public String getNif() {
        return nif;
    }

    @Override
    public String toString() {
        return name;
    }
}
