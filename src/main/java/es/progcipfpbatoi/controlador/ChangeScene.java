package es.progcipfpbatoi.controlador;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ChangeScene {

    public static void change(Stage stage, GenericController controller, String path_to_view_file) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(ChangeScene.class.getResource(path_to_view_file));
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 400, 500);
            stage.setScene(scene);
            stage.setResizable(true);
            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
