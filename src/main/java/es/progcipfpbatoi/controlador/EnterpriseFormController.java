package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.exceptions.ValidationException;
import es.progcipfpbatoi.modelo.dao.EnterpriseDAOInterface;
import es.progcipfpbatoi.modelo.dto.Enterprise;
import es.progcipfpbatoi.validator.FieldValidator;
import es.progcipfpbatoi.validator.Validador;
import es.progcipfpbatoi.validator.Validations;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import net.synedra.validatorfx.Validator;

import java.util.List;


public class EnterpriseFormController implements GenericController {

    private static final String NOMBRE_TEXTFIELD_NIF = "Nif";

    private EnterpriseDAOInterface databaseEnterpriseDAO;

    @FXML
    private GridPane root;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField nifTextField;

    @FXML
    private TextField direccionTextField;

    @FXML
    private TextField ciudadTextField;

    @FXML
    private ComboBox<String> provinciaComboBox;

    @FXML
    private ComboBox<String> paisComboBox;

    private Validador validador;

    public EnterpriseFormController(EnterpriseDAOInterface enterpriseDAO) {
        this.databaseEnterpriseDAO = enterpriseDAO;
        this.validador = new Validador();
    }

    @Override
    public void initialize() {
        // Se rellena el combobox de provincias
        List<String> provincias = databaseEnterpriseDAO.findAllDistinctDataFromField("province");
        provinciaComboBox.getItems().addAll(provincias);

        // Se rellena el combobox de paises
        List<String> paises = databaseEnterpriseDAO.findAllDistinctDataFromField("country");
        paisComboBox.getItems().addAll(paises);

        // Validaciones....

        validador.addValidation(nameTextField, FieldValidator.textFieldContieneInformacion(nameTextField));
        validador.addValidation(nifTextField, FieldValidator.textFieldContieneNif(nifTextField));
        validador.addValidation(direccionTextField, FieldValidator.textFieldContieneInformacion(direccionTextField));
        validador.addValidation(ciudadTextField, FieldValidator.textFieldContieneInformacion(ciudadTextField));
        validador.addValidation(provinciaComboBox, FieldValidator.comboBoxTieneUnItemSeleccionado(provinciaComboBox));
        validador.addValidation(paisComboBox, FieldValidator.comboBoxTieneUnItemSeleccionado(paisComboBox));
    }

    @FXML
    private void handleButtonAddEnterprise() {

        if (validador.valida()) {
            Enterprise enterprise = new Enterprise(
                    nameTextField.getText(),
                    direccionTextField.getText(),
                    ciudadTextField.getText(),
                    provinciaComboBox.getSelectionModel().getSelectedItem(),
                    paisComboBox.getSelectionModel().getSelectedItem(),
                    nifTextField.getText());
            databaseEnterpriseDAO.save(enterprise);
        } else {
            System.out.println("El formulario contiene errores");
        }
    }
}

