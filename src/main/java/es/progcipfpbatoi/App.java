package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.*;
import es.progcipfpbatoi.modelo.dao.DBEnterpriseDAO;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    @Override
    public void start(Stage stage) throws IOException {
        DBEnterpriseDAO dbEnterpriseDAO = new DBEnterpriseDAO();
        GenericController controller = new EnterpriseFormController(dbEnterpriseDAO);
        ChangeScene.change(stage, controller,"/vista/enterprise_form.fxml");
    }

    public static void main(String[] args) {
        launch();
    }
}
