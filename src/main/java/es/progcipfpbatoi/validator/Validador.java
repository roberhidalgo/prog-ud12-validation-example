package es.progcipfpbatoi.validator;

import javafx.scene.Node;
import net.synedra.validatorfx.Check;
import net.synedra.validatorfx.Validator;

import java.util.function.Consumer;

public class Validador {

    private Validator validator;

    public Validador() {
        this.validator = new Validator();
    }

    public void addValidation(Node componenteAValidar, Consumer<Check.Context> metodoValidacion) {
        validator.createCheck()
                .withMethod(metodoValidacion)
                .decorates(componenteAValidar);
    }

    public boolean valida() {
        return this.validator.validate();
    }

}
