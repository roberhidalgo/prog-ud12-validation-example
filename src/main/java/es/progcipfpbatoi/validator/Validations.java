package es.progcipfpbatoi.validator;


public class Validations {

    private static final String REGEXP_NIF = "^\\d{8}[TRWAGMYFPDXBNJZSQVHLCKE]$";

    public static boolean validateNIF(String nif) {
        return nif.matches(REGEXP_NIF);
    }

    public static boolean validateNotBlank(String text) {return !text.trim().isBlank();}

}
