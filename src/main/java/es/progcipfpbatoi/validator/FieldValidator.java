package es.progcipfpbatoi.validator;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import net.synedra.validatorfx.Check;

import java.util.function.Consumer;

/**
 * Essta clase aglutinará todas las posibles comprobaciones para diferentes componentes (textfield, combobox...)
 * a través de métodos static. Se apoyará en la clase Validations
 */
public class FieldValidator {

    public static Consumer<Check.Context> textFieldContieneInformacion(TextField textField) {
        return (c-> {
            if (!Validations.validateNotBlank(textField.getText())) {
                c.error("Este campo no puede estar vacío");
            }
        });
    }

    public static Consumer<Check.Context> textFieldContieneNif(TextField textField) {
        return (c-> {
            if (!Validations.validateNIF(textField.getText())) {
                c.error("El nif no es correcto");
            }
        });
    }

    public static Consumer<Check.Context> comboBoxTieneUnItemSeleccionado(ComboBox comboBox) {
        return (c-> {
            if (comboBox.getSelectionModel().getSelectedItem() == null) {
                c.error("Debe seleccionar un dato en este campo");
            }
        });
    }
}
